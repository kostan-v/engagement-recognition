import pickle
import unittest
from os.path import basename, join

import cv2
import numpy as np
from tqdm import tqdm

from data.lab_data import DATA_FOLDER, TEST_DATA_FOLDER
from tools.io import get_input_stream, get_output_stream


class TestReadAndWrite(unittest.TestCase):

    def test_read_and_write_motion(self):

        filename = join(DATA_FOLDER, 'Test 4/clips/P12_Interactive Site 1.mp4')

        # Setup motion
        motion_params = {
            'pyr_scale': 0.5,
            'levels': 3,
            'winsize': 15,
            'iterations': 3,
            'poly_n': 5,
            'poly_sigma': 1.2,
            'flags': 0,
        }

        # Setup input
        vc = cv2.VideoCapture(filename)

        self.assertTrue(vc.isOpened())  # Failed to open video!

        nframes = vc.get(cv2.CAP_PROP_FRAME_COUNT)
        fps = vc.get(cv2.CAP_PROP_FPS)
        frame_size = (int(vc.get(cv2.CAP_PROP_FRAME_WIDTH)), int(vc.get(cv2.CAP_PROP_FRAME_HEIGHT)))

        # Setup output
        with open(join(TEST_DATA_FOLDER, 'motion.pickle'), 'wb') as outfile:
            writer = pickle.Pickler(outfile, protocol=4)
            self.assertTrue(writer)     # Failed to init Pickler!

            # Prev frame and buffers init
            ret, frame = vc.read()
            if not ret:
                raise Exception('Failed to read 0-th frame!')
            gray_prev = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray_actual = np.zeros_like(gray_prev)

            # Processing
            # range(nframes)
            for frame_i in tqdm(range(1, 300), desc="proc \'%s\'" % basename(filename), total=nframes, unit='frame'):
                ret, frame = vc.read(frame)
                if ret:
                    cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY, dst=gray_actual)
                    optflow = cv2.calcOpticalFlowFarneback(gray_prev, gray_actual, None, **motion_params)
                    writer.dump(optflow)
                    tmp = gray_actual
                    gray_actual = gray_prev
                    gray_prev = tmp
                else:
                    if frame_i < nframes - 1:
                        raise Exception('Failed to read {}-th frame!'.format(frame_i))

        # Release
        vc.release()

    def test_read_and_write_basic(self):

        filename = join(DATA_FOLDER, 'Test 4/clips/P12_Interactive Site 1.mp4')

        vc = cv2.VideoCapture(filename)

        if not vc.isOpened():
            raise Exception('Failed to open video!Z')

        nframes = vc.get(cv2.CAP_PROP_FRAME_COUNT)
        fps = vc.get(cv2.CAP_PROP_FPS)
        frame_size = (int(vc.get(cv2.CAP_PROP_FRAME_WIDTH)), int(vc.get(cv2.CAP_PROP_FRAME_HEIGHT)))

        fourcc = cv2.VideoWriter_fourcc(*'XVID')

        fps_out = 3
        writer = cv2.VideoWriter(join(TEST_DATA_FOLDER, 'rw.avi'), fourcc=fourcc,
                                 fps=fps_out, frameSize=frame_size)

        if not writer.isOpened():
            raise Exception('Failed to open writer!')

        win_size = int(fps)                             # ~1 sec
        win_step = int(win_size / fps_out)   # ~1/3 sec

        # range(nframes)
        for frame_i in tqdm(range(3000), desc="proc \'%s\'" % basename(filename), total=nframes, unit='frame'):
            ret, frame = vc.read()
            if ret:
                writer.write(frame)
            else:
                if frame_i < nframes - 1:
                    raise Exception('Failed to read {}-ith frame!'.format(frame_i))

        writer.release()
        vc.release()


class TestReadAndWriteWrapper(unittest.TestCase):

    def test_cv2(self):
        filename_in = join(DATA_FOLDER, 'Test 4/clips/P12_Interactive Site 1.mp4')
        reader = get_input_stream(filename_in, 'cv')
        frame_size = reader.frame_size
        fps = reader.fps
        pix_fmt = 'bgr'

        filename_out = join(TEST_DATA_FOLDER, 'rw_cv2.avi')
        writer = get_output_stream(filename_out, frame_size, fps, pix_fmt, 'cv')

        nframes = 3000
        for frame_i in tqdm(range(nframes), desc="proc \'%s\'" % basename(filename_in), total=nframes, unit='frame'):
            ret, frame = reader.read()
            if ret:
                writer.write(frame)
            else:
                if frame_i < nframes - 1:
                    self.assertTrue(False, msg='Failed to read {}-ith frame!'.format(frame_i))

        writer.release()
        reader.release()

    def test_cv2_gray(self):
        filename_in = join(DATA_FOLDER, 'Test 4/clips/P12_Interactive Site 1.mp4')
        reader = get_input_stream(filename_in, 'cv')
        frame_size = reader.frame_size
        fps = reader.fps
        pix_fmt = 'gray'

        filename_out = join(TEST_DATA_FOLDER, 'rw_cv2_gray.avi')
        writer = get_output_stream(filename_out, frame_size, fps, pix_fmt, 'cv')

        nframes = 3000
        for frame_i in tqdm(range(nframes), desc="proc \'%s\'" % basename(filename_in), total=nframes, unit='frame'):
            ret, frame = reader.read()
            if ret:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                writer.write(frame)
            else:
                if frame_i < nframes - 1:
                    self.assertTrue(False, msg='Failed to read {}-ith frame!'.format(frame_i))

        writer.release()
        reader.release()


def test_cv():
    filename = join(DATA_FOLDER, 'Test 4/clips/P12_Interactive Site 1.mp4')

    vc = cv2.VideoCapture(filename)

    if not vc.isOpened():
        raise Exception('Nenacitalo video!')

    cv2.namedWindow('video')

    while True:
        ret, frame = vc.read()
        if not ret:
            break
        cv2.imshow('video', frame)

    cv2.destroyAllWindows()

    vc.release()
