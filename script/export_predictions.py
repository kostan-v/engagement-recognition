import argparse
import logging
import pickle
from os.path import join
from keras.models import load_model

from tools.log_config import setup_logging
from model.util import ModelParameters
from data.lab_data import CACHE_FOLDER, cache_output
from data.dataset import Dataset, GrayDataset, GazeDataset, GrayFlowDataset
from data.data_sampling import SequentialSampler, UniformClipSampler, UniformFlowClipSampler
from data.util import get_steps


logger = setup_logging(logging, __name__)
logging.getLogger('PIL').setLevel(logging.WARNING)     # DISABLE [PIL.PngImagePlug] [DEBUG] logs


CLIPS_FILEPATH = join(CACHE_FOLDER, 'clips.pickle')

def get_cache_data_filepath(params):
    if params.data_type == 'gray':
        if params.clip_length > 0:
            if params.sample_every_i_frame == 6:
                filename = 'clips.pickle'
            elif params.sample_every_i_frame == 3:
                filename = 'clipsPilot'
            else:
                raise NotImplementedError
        else:
            filename = 'imgs.pickle'
    elif params.data_type == 'flow':
        if params.clip_length > 0:
            filename = 'clipsFlow.pickle'
        else:
            raise NotImplementedError
    elif params.data_type == 'gaze':
        if params.clip_length > 0:
            filename = 'clipsGaze.pickle'
        else:
            raise NotImplementedError
    return join(CACHE_FOLDER, 'clips.pickle')

def cache_data(df, params):
    with open(get_cache_data_filepath(params), 'wb') as f:
        pickle.dump(df, f)

def load_data(params):
    with open(get_cache_data_filepath(params), 'rb') as f:
        return pickle.load(f)


def evaluate(model_name):
    model_filepath = Dataset.create_chckpnt_filepath(model_name)
    params = ModelParameters.load(model_name)
    model = load_model(model_filepath)

    # Load data
    if params.data_type == 'gray':
        if params.clip_length > 1:
            sampler = UniformClipSampler(params.clip_length,
                                         params.sample_every_i_frame,
                                         params.overlap,
                                         sampling_start_offset=params.sample_every_i_frame//2)  # clips sampled a half period
                                                                                                # should be the most variant
        else:
            sampler = SequentialSampler()
        dataset = GrayDataset(sampler)
    elif params.data_type == 'flow':
        sampler = UniformFlowClipSampler(params.clip_length,
                                         params.sample_every_i_frame,
                                         params.overlap,
                                         sampling_start_offset=params.sample_every_i_frame//2)  # clips sampled a half period
                                                                                                    # should be the most variant
        dataset = GrayFlowDataset(sampler)
    elif params.data_type == 'gaze':
        sampler = UniformClipSampler(params.clip_length,
                                         params.sample_every_i_frame,
                                         params.overlap,
                                         sampling_start_offset=params.sample_every_i_frame//2)  # clips sampled a half period
                                                                                                # should be the most variant
        dataset = GazeDataset(sampler)
    else:
        raise NotImplementedError

    print(str(params))

    # test_data = dataset.get_data()
    try:
        test_data = load_data(params)
    except FileNotFoundError:
        test_data = dataset.get_samples(params.batch_size)

    logger.info('Creating predictions for ' + model_name)
    pred_filepath = Dataset.create_predictions_filepath(model_name)

    gen = dataset.generate_batches(test_data, params.batch_size)
    steps = len(test_data)
    # gen, steps = dataset.get_generator(params.batch_size)
    probabilities = model.predict_generator(gen, steps=steps)

    cache_data(test_data,  params)

    with open(pred_filepath, 'wb') as f:
        pickle.dump(probabilities, f)

    logger.info('Predictions for %s created and saved to \"%s\"' % (model_name, pred_filepath))

    return probabilities


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Cross validate model for all '
                                                 'participants and segments.')
    parser.add_argument('--model_name', nargs='+')
    args = parser.parse_args()

    if isinstance(args.model_name, list):
        for name in args.model_name:
            evaluate(name)
    else:
        evaluate(args.model_name)
