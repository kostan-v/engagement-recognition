import logging

from data.dataset import GazeDataset, GrayDataset
from data.data_sampling import UniformClipSampler, RandomClipSampler
from model.model import get_model
from model.util import get_callbacks, ModelParameters, train
from tools.log_config import setup_logging

logger = setup_logging(logging, __name__)


def main():
    epochs = 15
    learning_rate = 0.0001

    model_type = 'lstm-gaze'
    model_version = 7
    data_type = 'gaze'
    overlap = 0.25
    sample_every_i_frame = 1
    clip_length = 10  # seconds

    # Set batch size
    batch_size = 32

    # Remove missing gaze data
    max_missing = 0.20

    # Load data
    # sampler = UniformClipSampler(clip_length, sample_every_i_frame, overlap)
    sampler = RandomClipSampler(clip_length, sample_every_i_frame, overlap)
    if data_type == 'gray':
        data = GrayDataset(sampler)
    elif data_type == 'gaze':
        data = GazeDataset(sampler, max_missing)
    else:
        raise NotImplementedError

    input_shape = data.get_input_shape()

    # Do cross-validation
    for participant in data.participant_set:
        model_name = '{}_v{:02d}_cvsplit_{}'.format(model_type, model_version,
                                                    participant)
        params = ModelParameters(
            epochs=epochs,
            model_type=model_type,
            model_version=model_version,
            model_name=model_name,
            data_type=data_type,
            overlap=overlap,
            sample_every_i_frame=sample_every_i_frame,
            clip_length=clip_length,
            batch_size=batch_size)
        params.save()

        callbacks = get_callbacks(
            model_name=model_name,
            batch_size=batch_size)

        model = get_model(model_type, input_shape, learning_rate)

        test_participants = {participant}
        train_participants = data.participant_set - test_participants

        train(
            dataset=data,
            test_participants=test_participants,
            train_participants=train_participants,
            batch_size=batch_size,
            model=model,
            epochs=epochs,
            callbacks=callbacks
        )


if __name__ == "__main__":
    main()
