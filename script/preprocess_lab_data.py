from data.lab_data import LabData, get_label, CLASSES, DATA_FOLDER, join


def get_reports_mask(df):
    mask = df['KeyPressEvent'] == 'Q'

    mask_copy = mask.copy()

    for i, row in df[mask].iterrows():
        report_time = row['RecordingTimestamp']
        mask_copy = mask_copy | ( (report_time - 10000 < df['RecordingTimestamp']) & (df['RecordingTimestamp'] <= report_time))

    return mask_copy


def export_fixation():
    lab_data = LabData()
    fixation_cols = ['FixationIndex', 'SaccadeIndex', 'GazeEventType', 'GazeEventDuration']
    cols = ['RecordingName', 'SegmentName', 'ParticipantName', 'KeyPressEvent', 'RecordingTimestamp'] + lab_data.GAZE_FIELDS + fixation_cols
    label_sr = get_label(segment='MWSelfReports')

    with open(join(DATA_FOLDER, 'fixations.csv'), 'w') as f:

        cols_to_write = ['RecordingName', 'SegmentName', 'ParticipantName', 'label', 'RecordingTimestamp'] + lab_data.GAZE_FIELDS + fixation_cols

        # Write header
        f.write(','.join(cols_to_write) + '\n')

        # Search data.tsv file for each test
        for test in lab_data.test_names:
            reader = lab_data._get_data_reader(test, cols)

            # Sequentially process chunks
            for chunk in reader:
                # Get targets
                chunk['label'] = chunk['SegmentName'].apply(lambda x: get_label(segment=x))
                chunk = chunk[chunk['label'] >= 0]
                # Mark self reports
                if label_sr >= 0:
                    mask = get_reports_mask(chunk)
                    chunk.loc[mask, 'SegmentName'] = 'MWSelfReports_' + chunk.loc[mask, 'SegmentName']
                    chunk.loc[mask, 'label'] = label_sr

                chunk['label'] = chunk['label'].apply(lambda x: CLASSES[x])

                chunk[cols_to_write].to_csv(f, ',', header=False, index=False)

    print('All done.')


if __name__ == '__main__':
    export_fixation()
