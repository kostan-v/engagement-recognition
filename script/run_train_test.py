import logging
import os

from keras.models import load_model

from data.dataset import Dataset, GazeDataset, GrayDataset, GrayFlowDataset
from data.data_sampling import RandomClipSampler, RandomSampler
from model.model import get_model
from model.util import get_callbacks, train, ModelParameters, \
    make_model_name
from tools.log_config import setup_logging

logger = setup_logging(logging, __name__)
logging.getLogger('PIL').setLevel(logging.WARNING)     # DISABLE [PIL.PngImagePlug] [DEBUG] logs


def main():
    epochs = 35
    learning_rate = 0.0001
    batch_size = 16

    model_type = 'lstm-gaze'
    model_version = 9
    model_name = make_model_name(model_type, model_version)
    data_type = 'gaze'
    overlap = 0.25
    sample_every_i_frame = 1
    clip_length = 10  # seconds

    participant = 'P01'

    # Remove missing gaze data
    max_missing = 1.0 # 0.20

    balanced = True

    # Store params
    params = ModelParameters(
        epochs=epochs,
        model_type=model_type,
        model_version=model_version,
        model_name=model_name,
        data_type=data_type,
        overlap=overlap,
        sample_every_i_frame=sample_every_i_frame,
        clip_length=clip_length,
        batch_size=batch_size,
        learning_rate=learning_rate,
        max_missing=max_missing,
        balanced=balanced,
        val_participant=participant)

    params.save()

    # Load data
    sampler = RandomClipSampler(clip_length, sample_every_i_frame, overlap) if clip_length > 0 else RandomSampler()

    if params.data_type == 'gray':
        data = GrayDataset(sampler)
    elif params.data_type == 'gaze':
        data = GazeDataset(sampler, params.max_missing)
    elif params.data_type == 'flow':
        data = GrayFlowDataset(sampler)
    else:
        raise NotImplementedError

    callbacks = get_callbacks(
        model_name=model_name,
        batch_size=batch_size)

    input_shape = data.get_input_shape()

    chckpnt_filepath = Dataset.create_chckpnt_filepath(model_name)
    if os.path.isfile(chckpnt_filepath):
        model = load_model(chckpnt_filepath)
    else:
        model = get_model(model_type, input_shape, learning_rate)

    test_participants = {participant}
    train_participants = data.participant_set - test_participants

    train(
        dataset=data,
        test_participants=test_participants,
        train_participants=train_participants,
        batch_size=batch_size,
        model=model,
        epochs=epochs,
        callbacks=callbacks
    )

    model.save(chckpnt_filepath + '_final')


if __name__ == "__main__":
    main()
