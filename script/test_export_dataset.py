import unittest
from os.path import join

from export_dataset import create_export_mappings, DATASET_FOLDER


class CreateExportMappingsTest(unittest.TestCase):

    def setUp(self):
        self.test_data = {
            'sequences': [
                ('rec1.mp4', 'partic1', 'segment1', 'segment1_000',0, 5, 1),
                ('rec1.mp4', 'partic1', 'segment2', 'MWSelfReports_segment2_000', 2, 3, 0),
                ('rec1.mp4', 'partic2', 'segment1', 'MWSelfReports_segment1_000', 2, 3, 0),
                ('rec1.mp4', 'partic2', 'segment2', 'segment2_000', 0, 5, 1),
                ('rec2.mp4', 'partic1', 'segment1', 'MWSelfReports_segment1_001', 2, 3, 0),
                ('rec2.mp4', 'partic2', 'segment1', 'segment1_000', 0, 5, 1),
            ],
            'dest_dir': DATASET_FOLDER
        }
        self.result = [
            ('rec1.mp4',
             [(0, 5, join(DATASET_FOLDER, '1', 'partic1', 'segment1_000')),
              (2, 3, join(DATASET_FOLDER, '0', 'partic1', 'MWSelfReports_segment2_000')),
              (2, 3, join(DATASET_FOLDER, '0', 'partic2', 'MWSelfReports_segment1_000')),
              (0, 5, join(DATASET_FOLDER, '1', 'partic2', 'segment2_000'))]),
            ('rec2.mp4',
             [(2, 3, join(DATASET_FOLDER, '0', 'partic1', 'MWSelfReports_segment1_001')),
              (0, 5, join(DATASET_FOLDER, '1', 'partic2', 'segment1_000'))])
        ]

    def test_basic(self):
        result = create_export_mappings(**self.test_data)
        self.assertListEqual(self.result, result)


if __name__ == '__main__':
    unittest.main()
