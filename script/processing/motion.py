import numpy as np
import cv2


def compute_optical_flow(frame1, frame2, *farneback_args, **farneback_kwargs):
    if len(frame1.shape) > 2:
        frame1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
    if len(frame2.shape) > 2:
        frame2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)

    if farneback_args and farneback_kwargs:
        flow = cv2.calcOpticalFlowFarneback(frame1, frame2, *farneback_args, **farneback_kwargs)
    elif farneback_args:
        flow = cv2.calcOpticalFlowFarneback(frame1, frame2, *farneback_args)
    elif farneback_kwargs:
        flow = cv2.calcOpticalFlowFarneback(frame1, frame2, **farneback_kwargs)
    else:
        flow = cv2.calcOpticalFlowFarneback(frame1, frame2, None, 0.5, 3, 5, 3, 5, 1.2, 0)

    return flow


def visualize_optical_flow(flow, hsv=None, win_name=None, block=False):
    mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])

    if hsv is None:
        hsv = np.zeros((flow.shape[0], flow.shape[1], 3), dtype=np.uint8)
        hsv[...,1] = 255
    hsv[..., 0] = ang * 180 / np.pi / 2
    hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

    if win_name:
        cv2.imshow(win_name, bgr)

        while block:
            k = cv2.waitKey(30) & 0xff
            if k == 27 or k == 13:
                break

    return bgr


def test_visual():
    cap = cv2.VideoCapture(0)

    if not cap.isOpened():
        raise Exception('WebCam not initialized!')

    ret, frame1 = cap.read()
    prvs = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
    hsv = np.zeros_like(frame1)
    hsv[..., 1] = 255

    pyr_scale = 0.5
    win_size = 10

    while (True):
        ret, frame2 = cap.read()
        next_frame = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)

        flow = compute_optical_flow(prvs, next_frame, None, pyr_scale, 3, win_size, 3, 5, 1.2, 0)

        visualize_optical_flow(flow, hsv=hsv, win_name="Flow", block=False)

        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break
        elif k == ord('a'):
            win_size = max(0, win_size - 1)
        elif k == ord('s'):
            win_size = min(30, win_size + 1)
        elif k == ord('q'):
            pyr_scale = max(0., pyr_scale - 0.1)
        elif k == ord('w'):
            pyr_scale = min(0.99, pyr_scale + 0.1)

        prvs = next_frame
    cap.release()
    cv2.destroyAllWindows()
