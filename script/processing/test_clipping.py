import unittest
from processing.clipping import *


class TestTimeWindowsClipper(unittest.TestCase):

    def setUp(self):
        self.crop_begin_data = {
            'segment_tws': [
                ('test1', 'partic1', 'segment1', 1, 4, 1),
                ('test1', 'partic1', 'segment1', 11, 13, 1),
                ('test1', 'partic1', 'segment1', 20, 22, 1),
            ],
            'self_reports_tws': [
                ('test1', 'partic1', 'segment1', 1, 2, 0),
                ('test1', 'partic1', 'segment1', 10, 11, 0),
                ('test1', 'partic1', 'segment1', 20, 21, 0),
            ],
            'result': [
                ('test1', 'partic1', 'segment1', 'MWSelfReports_segment1_000', 1, 2, 0),
                ('test1', 'partic1', 'segment1', 'segment1_000', 3, 4, 1),
                ('test1', 'partic1', 'segment1', 'MWSelfReports_segment1_001', 10, 11, 0),
                ('test1', 'partic1', 'segment1', 'segment1_001', 12, 13, 1),
                ('test1', 'partic1', 'segment1', 'MWSelfReports_segment1_002', 20, 21, 0),
                ('test1', 'partic1', 'segment1', 'segment1_002', 22, 22, 1),
            ],
        }

        self.crop_end_data = {
            'segment_tws': [
                ('test1', 'partic1', 'segment1', 1, 4, 1),
                ('test1', 'partic1', 'segment1', 11, 13, 1),
                ('test1', 'partic1', 'segment1', 20, 22, 1),
            ],
            'self_reports_tws': [
                ('test1', 'partic1', 'segment1', 3, 4, 0),
                ('test1', 'partic1', 'segment1', 13, 14, 0),
                ('test1', 'partic1', 'segment1', 21, 22, 0),
            ],
            'result': [
                ('test1', 'partic1', 'segment1', 'segment1_000', 1, 2, 1),
                ('test1', 'partic1', 'segment1', 'MWSelfReports_segment1_000', 3, 4, 0),
                ('test1', 'partic1', 'segment1', 'segment1_001', 11, 12, 1),
                ('test1', 'partic1', 'segment1', 'MWSelfReports_segment1_001', 13, 14, 0),
                ('test1', 'partic1', 'segment1', 'segment1_002', 20, 20, 1),
                ('test1', 'partic1', 'segment1', 'MWSelfReports_segment1_002', 21, 22, 0),
            ],
        }

        self.split_data = {
            'segment_tws': [
                ('test1', 'partic1', 'segment1', 0, 5, 1),
                ('test1', 'partic1', 'segment1', 10, 12, 1),
            ],
            'self_reports_tws': [
                ('test1', 'partic1', 'segment1', 2, 3, 0),
                ('test1', 'partic1', 'segment1', 11, 11, 0),
            ],
            'result': [
                ('test1', 'partic1', 'segment1', 'segment1_000', 0, 1, 1),
                ('test1', 'partic1', 'segment1', 'MWSelfReports_segment1_000', 2, 3, 0),
                ('test1', 'partic1', 'segment1', 'segment1_001', 4, 5, 1),
                ('test1', 'partic1', 'segment1', 'segment1_002', 10, 10, 1),
                ('test1', 'partic1', 'segment1', 'MWSelfReports_segment1_001', 11, 11, 0),
                ('test1', 'partic1', 'segment1', 'segment1_003', 12, 12, 1),
            ],
        }

        self.diff_segs_data = {
            'segment_tws': [
                ('test1', 'partic1', 'segment1', 0, 5, 1),
                ('test1', 'partic2', 'segment2', 0, 5, 1),
                ('test2', 'partic2', 'segment1', 0, 5, 1),
            ],
            'self_reports_tws': [
                ('test1', 'partic1', 'segment2', 2, 3, 0),
                ('test1', 'partic2', 'segment1', 2, 3, 0),
                ('test2', 'partic1', 'segment1', 2, 3, 0),
            ],
            'result': [
                ('test1', 'partic1', 'segment1', 'segment1_000',0, 5, 1),
                ('test1', 'partic1', 'segment2', 'MWSelfReports_segment2_000', 2, 3, 0),
                ('test1', 'partic2', 'segment1', 'MWSelfReports_segment1_000', 2, 3, 0),
                ('test1', 'partic2', 'segment2', 'segment2_000', 0, 5, 1),
                ('test2', 'partic1', 'segment1', 'MWSelfReports_segment1_001', 2, 3, 0),
                ('test2', 'partic2', 'segment1', 'segment1_000', 0, 5, 1),
            ],
        }

    def test_begin(self):
        clipper = TimeWindowsClipper(self.crop_begin_data['segment_tws'],
                                     self.crop_begin_data['self_reports_tws'])
        result = clipper.execute()
        self.assertListEqual(self.crop_begin_data['result'], result)

    def test_end(self):
        clipper = TimeWindowsClipper(self.crop_end_data['segment_tws'],
                                     self.crop_end_data['self_reports_tws'])
        result = clipper.execute()
        self.assertListEqual(self.crop_end_data['result'], result)

    def test_split(self):
        clipper = TimeWindowsClipper(self.split_data['segment_tws'],
                                     self.split_data['self_reports_tws'])
        result = clipper.execute()
        self.assertListEqual(self.split_data['result'], result)

    def test_diff_segs(self):
        clipper = TimeWindowsClipper(self.diff_segs_data['segment_tws'],
                                     self.diff_segs_data['self_reports_tws'])
        result = clipper.execute()
        self.assertListEqual(self.diff_segs_data['result'], result)


if __name__ == '__main__':
    unittest.main()
