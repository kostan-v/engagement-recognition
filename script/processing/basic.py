import numpy as np

try:
    import cv2
    def imread(filepath, as_grey=False):
        img = cv2.imread(filepath) if not as_grey else cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
        if img is None:
            raise ValueError('Could not load file \"{}\"'.format(img))
        return img
except ModuleNotFoundError:
    from skimage.io import imread


def crop_center(img,cropx,cropy):
    y,x = img.shape
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)
    return img[starty:starty+cropy, startx:startx+cropx]


def normalize(img):
    return (img / 255.).astype(np.float32)
