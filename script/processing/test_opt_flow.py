import cv2
import numpy as np
from data.lab_data import DATA_FOLDER
from os.path import join

def display_flow(winname, flow):
    hsv = np.zeros((*flow.shape[:2], 3), dtype=np.uint8)
    hsv[...,1] = 255

    mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
    hsv[...,0] = ang*180/np.pi/2
    hsv[...,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
    bgr = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)
    cv2.imshow(winname, bgr)


def display_gray(winname, gray):
    cv2.imshow(winname, gray)


def visual_test_flow_summing(video_path):
    cap = cv2.VideoCapture(video_path)
    ret, frame1 = cap.read()
    prvs = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
    prvs_flow = np.zeros((*prvs.shape, 3))
    orig = prvs
    accumulated_flow = np.zeros((*prvs.shape, 2))

    display_gray('orig gray', orig)

    read_another = True

    i = 0

    while read_another:
        ret, frame2 = cap.read()

        if not ret:
            break

        next = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)

        flow = cv2.calcOpticalFlowFarneback(prvs,next, None, 0.5, 3, 5, 3, 5, 1.2, 0)
        accumulated_flow += flow

        display_gray('act gray', next)
        display_flow('prvs flow', prvs_flow)
        display_flow('act flow', flow)
        display_flow('accum flow', accumulated_flow)

        i += 1
        print(i)

        while True:
            k = cv2.waitKey(30) & 0xff
            if k == 27:
                read_another = False
                break
            elif k in (13, 10):
                break
            elif k in (83, 114):
                accumulated_flow = flow
                orig = next
                display_gray('orig gray', orig)
                break

        prvs = next
        prvs_flow = flow

    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    visual_test_flow_summing(join(DATA_FOLDER, 'Test 1', 'usercams', 'P01_rec_01.mp4'))
