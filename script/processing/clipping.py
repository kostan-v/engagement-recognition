import logging

logger = logging.getLogger(__name__)


class TimeWindowsClipper:

    def __init__(self, segment_tws, self_report_tws):
        # Sort
        self.segment_tws = sorted(segment_tws, reverse=True)
        self.self_report_tws = sorted(self_report_tws, reverse=True)

        # Get first items
        self.segment_tw = self.segment_tws.pop() if len(self.segment_tws) > 0 else None
        self.self_report_tw = self.self_report_tws.pop() if len(self.self_report_tws) > 0 else None

        # Sequence counters
        self.seg_seq_i = 0
        self.seg_seq_key = self.segment_tw[2] if self.segment_tw is not None else None

        self.sr_seq_i = 0
        self.sr_seq_key = self.self_report_tw[2] if self.self_report_tw is not None else None

        self.filtered_tws = []

    def _inject_seg_sequence_id(self):
        seg_file, seg_participant, seg_seg, seg_begin, seg_end, seg_label = self.segment_tw

        if self.seg_seq_key != seg_seg:
            self.seg_seq_key = seg_seg
            self.seg_seq_i = 0

        sequence = '{}_{:03d}'.format(self.seg_seq_key, self.seg_seq_i)

        self.segment_tw = (seg_file, seg_participant, seg_seg, sequence,
                           seg_begin, seg_end, seg_label)
        self.seg_seq_i += 1

    def _inject_sr_sequence_id(self):
        sr_file, sr_participant, sr_seg, sr_begin, sr_end, sr_label = self.self_report_tw

        if self.sr_seq_key != sr_seg:
            self.sr_seq_key = sr_seg
            self.sr_seq_i = 0

        sequence = 'MWSelfReports_{}_{:03d}'.format(self.sr_seq_key,
                                                    self.sr_seq_i)

        self.self_report_tw = (sr_file, sr_participant, sr_seg, sequence,
                               sr_begin, sr_end, sr_label)
        self.sr_seq_i += 1

    def _next_segment(self):
        self._inject_seg_sequence_id()
        self.filtered_tws.append(self.segment_tw)
        self.segment_tw = self.segment_tws.pop() if self.segment_tws else None

    def _next_report(self):
        self._inject_sr_sequence_id()
        self.filtered_tws.append(self.self_report_tw)
        self.self_report_tw = self.self_report_tws.pop() if self.self_report_tws else None

    def _tws_overlapping(self):
        return ((self.segment_tw[2] < self.self_report_tw[3]) and
                (self.self_report_tw[2] < self.segment_tw[3]))

    def _clip_seg_tw(self):
        if not self.segment_tw or not self.self_report_tw:
            return
        seg_file, seg_participant, seg_seg, seg_begin, seg_end, seg_label = self.segment_tw
        sr_file, sr_participant, sr_seg, sr_begin, sr_end, sr_label = self.self_report_tw

        # If overlapping
        if (seg_participant == sr_participant) and (seg_seg == sr_seg) and\
                (seg_begin <= sr_end) and (sr_begin <= seg_end):

            # Clip front
            if sr_begin <= seg_begin:
                self.segment_tw = (seg_file, seg_participant, seg_seg,
                                   sr_end + 1, seg_end, seg_label)
            # Clip back
            elif sr_end >= seg_end:
                self.segment_tw = (seg_file, sr_participant, seg_seg, seg_begin,
                                   sr_begin - 1, seg_label)
            # Split
            else:
                left_seg_part = (seg_file, seg_participant, seg_seg, seg_begin,
                                 sr_begin - 1, seg_label)
                right_seg_part = (seg_file, seg_participant, seg_seg, sr_end + 1,
                                  seg_end, seg_label)

                self.segment_tws.append(right_seg_part)
                self.segment_tw = left_seg_part

    def execute(self):
        while (self.segment_tw is not None) or (self.self_report_tw is not None):

            # If no more reports
            # Note: Should be safe, because every sr is inside segment
            if self.self_report_tw is None:
                self._next_segment()
            else:
                # Clip segment time window if overlapping
                self._clip_seg_tw()

                # Next segment
                if self.segment_tw and self.segment_tw < self.self_report_tw:
                    self._next_segment()
                # Next report
                else:
                    self._next_report()

        return self.filtered_tws


def create_sequences(segment_tws, self_report_tws):
    """
    Remove parts of segment time windows which are overlapped by self report
    time windows.
    :param segment_tws: Time windows of segments.
    :param self_report_tws: Time windows of self reports.
    :return: List of remaining parts of segments and self report time windiws.
        Format: [ (record_filepath, participant, segment_name, sequence_name,
                   start_frame, end_frame, label), ... ]
    """
    clipper = TimeWindowsClipper(segment_tws, self_report_tws)

    return clipper.execute()
