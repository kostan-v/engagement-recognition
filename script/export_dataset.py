import logging
import argparse
from os import makedirs
from os.path import isfile, join, basename

import cv2
import pandas as pd
from numpy import concatenate, atleast_3d
from tqdm import tqdm

from data.lab_data import LabData, DATASET_FOLDER
from processing.clipping import create_sequences
from tools.io import get_input_stream
from tools.log_config import setup_logging

logger = setup_logging(logging, __name__)


def create_export_mappings(sequences, dest_dir):
    """
    Create src video to dest directory mappings for sequences.
    :param sequences:
    :param dest_dir: Path to destination directory.
    :return: List of mappings: [ (src_filepath,
                                  [(start_frame, end_frame, dest_dir), ...]),
                                 ...]
    """
    df = pd.DataFrame(sequences, columns=['record_filepath', 'participant',
                                          'segment_name', 'sequence_name',
                                          'start_frame', 'end_frame', 'label'])
    mappings = [
        (record_filepath,
         [(row['start_frame'], row['end_frame'],
           join(dest_dir, str(row['label']), row['participant'],
                        row['sequence_name']))
          for _, row in rec_group.iterrows()])
        for record_filepath, rec_group in df.groupby('record_filepath')
    ]
    return mappings


class RgbExporter:

    def __init__(self, ext, imwrite_params):
        self.ext = ext
        self.imwrite_params = imwrite_params

    def export(self, img, img_index, dest_dirpath):
        filepath = join(dest_dirpath, '{:05d}.{}'.format(img_index, self.ext))
        if not isfile(filepath):
            cv2.imwrite(filepath, img, self.imwrite_params)


class GrayExporter:
    def __init__(self, ext, imwrite_params):
        self.ext = ext
        self.imwrite_params = imwrite_params

    def export(self, img, img_index, dest_dirpath):
        filepath = join(dest_dirpath, '{:05d}_gray.{}'.format(img_index, self.ext))
        if not isfile(filepath):
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            cv2.imwrite(filepath, img, self.imwrite_params)


class FlowExporter:
    """
    Exports Optical flow computed by Farneback's algorithm as well as the next
    image. Output channels (dx, dy, gray).
    """
    def __init__(self, ext, imwrite_params):
        self.ext = ext
        self.imwrite_params = imwrite_params
        self.prev_img = None

    def export(self, img, img_index, dest_dirpath):
        filepath = join(dest_dirpath, '{:05d}_flow.{}'.format(img_index, self.ext))
        if not isfile(filepath):
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            if self.prev_img is None:
                self.prev_img = img
            flow = cv2.calcOpticalFlowFarneback(self.prev_img, img, None, 0.5,
                                                3, 5, 3, 5, 1.2, 0)
            flow_w_gray = concatenate((flow, atleast_3d(img)), axis=2)
            cv2.imwrite(filepath, flow_w_gray, self.imwrite_params)


def get_exporter(export_type, ext, imwrite_params):
    """
    :param export_type: Type of image that will be exported.
                'rgb' - RGB image
                'gray' - Grayscale image
                'flow' - Image containing Optical flow and grayscale intensities -> [dx, dy, I]
    """
    if export_type == 'rgb':
        exporter = RgbExporter(ext, imwrite_params)
    elif export_type == 'gray':
        exporter = GrayExporter(ext, imwrite_params)
    elif export_type == 'flow':
        exporter = FlowExporter(ext, imwrite_params)
    else:
        raise ValueError('Invalid value \'%s\' for argument export_type!' % export_type)


def export_sequence_images(src_filepath, sequences_to_export, exporter):
    """
    Export sequences of PNG images from video.
    :param src_filepath: Path to record which will be exported.
    :param sequences_to_export: [(start_frame, end_frame, dest_dir), ...]
    :param exporter: Exporter object which handles exporting BGR image to file in specific data format.
    """

    # Sort sequences
    sequences_to_export = sorted(sequences_to_export)

    # Init reader
    reader = get_input_stream(src_filepath)

    if not reader.is_opened():
        logger.error('Failed to open {}'.format(src_filepath))
        return

    logger.info('Exporting images from {}'.format(src_filepath))

    for start, end, dest_dirpath in sequences_to_export:

        # Create needed directories
        makedirs(dest_dirpath, exist_ok=True)

        # Move cursor to start
        reader.go_to_frame(start)

        # Export until end + 1 (end is included)
        for i in tqdm(range(end - start + 1), desc='Exporting', unit='img'):
            ret, img = reader.read()

            if ret:
                exporter.export(img, i, dest_dirpath)
            else:
                logger.error('Unexpected input read fail! File({}) Frame({})'
                             .format(src_filepath, i))
                break

        logger.info('Sequence exported to {}'.format(basename(dest_dirpath)))

    reader.release()


def main(export_type):
    # Size of time window for mind wandering self reports
    sr_tw_size_in_ms = 10000

    data = LabData()

    logger.info('Loading time windows of segments...')
    segment_tws = data.get_segment_tws()

    logger.info('Loading time windows of self reported mind wandering...')
    self_report_tws = data.get_self_report_tws(sr_tw_size_in_ms)

    logger.info('Annotating time windows...')
    segment_tws, self_report_tws = data.annotate_tws(segment_tws,
                                                     self_report_tws)

    logger.info('Clipping segments time windows...')
    sequences = create_sequences(segment_tws, self_report_tws)

    logger.info('Creating src video to dest directory mappings for sequences.')
    export_mappings = create_export_mappings(sequences, DATASET_FOLDER)

    logger.info('Exporting images from records...')

    # Setup params for exporting image
    ext = 'png'
    imwrite_params = (cv2.IMWRITE_PNG_COMPRESSION, 9)

    exporter = get_exporter(export_type, ext, imwrite_params)

    for src_filepath, sequences in export_mappings:
        export_sequence_images(src_filepath, sequences, exporter=exporter)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Export dataset.')
    parser.add_argument('export_type', choices=['gray','rgb','flow'],
                        help='Type of data to be exported')
    args = parser.parse_args()
    main(args.export_type)
