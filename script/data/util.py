import re
import os
import numpy as np


def targets_to_onehot(targets, classes):
    onehot = np.eye(len(classes))
    onehot_index = {cls: idx for idx, cls in enumerate(classes)}
    get_onehot = lambda target: onehot[onehot_index[target]]

    # Convert targets to Onehot
    return targets.apply(get_onehot)


def onehot_to_targets(onehot):
    # Convert targets to Onehot
    return onehot.apply(lambda x: np.where(x == np.max(x))[0][0])


def get_sampling_step(win_length, overlap):
    return int(win_length * (1 - overlap))


def seconds_to_frames(secs, fps, sample_every_i_frame):
    return int(secs * fps / sample_every_i_frame)


def get_steps(elements, batch_size):
    return int(np.ceil(elements / batch_size))
