import unittest
from lab_data import *


class TestGetLabel(unittest.TestCase):

    def test_segments(self):
        for segment in SEGMENTS:
            label = SEGMENT_LABEL[segment]
            if label >= 0:
                self.assertEqual(get_label(segment=segment), label)
            else:
                with self.assertRaises(SegmentHasNoLabelException):
                    get_label(segment=segment)

    def test_filenames(self):
        fp = ClipFilenamePicker()
        filenames = fp.get_filenames()

        for filename in filenames:
            try:
                get_label(filename=filename)
            except Exception as ex:
                self.assertIsInstance(ex, SegmentHasNoLabelException)


if __name__ == '__main__':
    unittest.main()