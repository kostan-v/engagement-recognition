from abc import ABC, abstractmethod

from data.util import seconds_to_frames, get_sampling_step
import pandas as pd
import numpy as np


class DataSampler(ABC):

    @abstractmethod
    def get_epoch_samples(self, sources):
        raise NotImplementedError

    @abstractmethod
    def generate_samples(self, df):
        raise NotImplementedError

    @abstractmethod
    def sample_size(self):
        raise NotImplementedError


class BasicSampler(DataSampler):
    """Abstract class for sampling single sources."""
    @abstractmethod
    def _order_samples(self, frame_lookup_tbl):
        raise NotImplementedError

    def get_epoch_samples(self, sources):
        return sources.apply(lambda x: len(x)).sum()

    def generate_samples(self, df):
        df = df.copy()

        # Create lookup table for frames
        frame_lookup_tbl = []
        for i, row in df.iterrows():
            clip_count = len(row.sources)
            for frame in range(clip_count):
                frame_lookup_tbl.append((i, frame))

        while True:
            for row_i, frame in self._order_samples(frame_lookup_tbl):
                row = df.loc[row_i]
                yield pd.Series({'frames': row.sources[frame],
                                 'targets': row.targets,
                                 'participant': row.participants,
                                 'segment': row.segments})

    def sample_size(self):
        return 1


class SequentialSampler(BasicSampler):
    """This sampler is used for sequentially sampling single frames or feature records."""

    def _order_samples(self, frame_lookup_tbl):
        return frame_lookup_tbl


class RandomSampler(BasicSampler):
    """This sampler is used for randomly sampling single frames or feature records."""

    def _order_samples(self, frame_lookup_tbl):
        return np.random.permutation(frame_lookup_tbl)


class ClipSampler(DataSampler):

    def __init__(self, clip_length, sample_every_i_frame, overlap):
        """
        :param clip_length: Lenght of input clips. (in seconds)
        :param sample_every_i_frame: source fps
        :param overlap: Fraction of two clip overlap with respect to clip length.
                        In RandomDataSampler it is used only for max_possible_samples.
        """
        self.clip_length_sec = clip_length
        self.sample_every_i_frame = sample_every_i_frame
        self.clip_length = seconds_to_frames(self.clip_length_sec, 30,
                                             self.sample_every_i_frame)
        self.overlap = overlap

    def set_fps(self, fps):
        self.clip_length = seconds_to_frames(self.clip_length_sec, fps,
                                             self.sample_every_i_frame)

    def get_epoch_samples(self, sources):
        return sources.apply(self.max_possible_samples).sum()

    def max_possible_samples(self, source):
        """
        Get maximum steps that can be sampled given clip_length and overlap
        """
        return int(np.ceil(
            (len(source) - (self.clip_length - 1))
            / (
                get_sampling_step(self.clip_length, self.overlap)
                * self.sample_every_i_frame
            )
        ))

    def sample_size(self):
        return self.clip_length


class UniformClipSampler(ClipSampler):
    def __init__(self, clip_length, sample_every_i_frame, overlap, sampling_start_offset=0):
        """
        :param clip_length: Lenght of input clips. (in seconds)
        :param sample_every_i_frame: source fps
        :param overlap: Fraction of two clip overlap with respect to clip length.
        """
        super().__init__(clip_length, sample_every_i_frame, overlap)
        self.step = None
        self.max_offset = None
        self.offset_end = None
        self.seed = sampling_start_offset

    def set_fps(self, fps):
        super().set_fps(fps)
        self.step = get_sampling_step(self.clip_length, self.overlap)
        self.offset_end = self.step * self.sample_every_i_frame

    def _subsample_frames(self, source):
        return source[::self.sample_every_i_frame]

    def generate_samples(self, df):
        while True:
            clips = []
            for i, row in df.iterrows():
                source_clips = self.sample_source(row.sources)
                clips.extend([pd.Series({'frames': clip,
                                         'targets': row.targets,
                                         'participant': row.participants,
                                         'segment': row.segments})
                                for clip in source_clips])

            for clip in clips:
                yield clip

            self.seed += 1

    def sample_source(self, source):
        seed = self.seed
        # 2nd line: Handle small sequences that can have just 1 clip
        seed %= min(self.offset_end,
                    max(len(source) - (self.clip_length - 1) * self.sample_every_i_frame,
                        1))  # Div by zero prevent

        # Subsample frames
        source = self._subsample_frames(source[seed:])

        # Sample clips
        clips = [source[i: i + self.clip_length]
                 for i in range(0, len(source) -
                                (self.clip_length - 1), self.step)]
        return clips


class UniformFlowClipSampler(UniformClipSampler):

    def _subsample_frames(self, source):
        first_frame = source[0]

        # Group flow frames
        source = [source[i: i + self.sample_every_i_frame]
                  for i in range(1, len(source), self.sample_every_i_frame)]

        source.insert(0, [first_frame])

        return source


class RandomClipSampler(ClipSampler):

    def generate_samples(self, df):
        df = df.copy()

        # Create lookup table for clip start frames
        start_lookup_tbl = []
        for i, row in df.iterrows():
            clip_count = len(row.sources) - \
                         (self.clip_length * self.sample_every_i_frame - 1)
            for frame in range(clip_count):
                start_lookup_tbl.append((i, frame))

        # Init "clip length" in original fps
        end_offset = self.clip_length * self.sample_every_i_frame

        while True:
            row_i, frame = start_lookup_tbl[np.random.randint(0, len(start_lookup_tbl))]
            row = df.loc[row_i]
            yield pd.Series({'frames': row.sources[frame: frame + end_offset: self.sample_every_i_frame],
                             'targets': row.targets,
                             'participant': row.participants,
                             'segment': row.segments})
