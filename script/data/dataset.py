import numpy as np
import pandas as pd

from sklearn.utils import class_weight

import logging
from abc import ABC, abstractmethod
from datetime import datetime
from os.path import join, dirname

from .lab_data import DATASET_FOLDER, LabData, get_segment_name
from .util import targets_to_onehot, onehot_to_targets, get_steps
from processing.basic import normalize, imread
from tools.path import get_filepaths, get_subdirs

logger = logging.getLogger(__name__)


MODEL_DATA_FOLDER = join(dirname(dirname(DATASET_FOLDER)), 'model')
MODEL_CHECKPOINT_FOLDER = join(MODEL_DATA_FOLDER, 'checkpoints')
TENSORBOARD_FOLDER = join(MODEL_DATA_FOLDER, 'tensorboard')
MODEL_LOG_FOLDER = join(MODEL_DATA_FOLDER, 'logs')
MODEL_EVAL_FOLDER = join(MODEL_DATA_FOLDER, 'eval')


class Dataset(ABC):

    fps = None
    input_shape = ()

    def __init__(self, sampler):
        """
        Wrapper class for dataset.
        :param sampler: DataSampler object which handles creating clips.
        """
        self.root_dirpath = DATASET_FOLDER
        self.sampler = sampler
        if sampler.sample_size() > 1:
            self.sampler.set_fps(self.fps)
        self.df = None
        self.participant_set = None
        self.segment_set = None
        self.class_set = None
        self.load()

    @abstractmethod
    def _preprocess(self, source):
        raise NotImplementedError

    @abstractmethod
    def _data_iterator(self):
        raise NotImplementedError

    def load(self):
        sources = []
        targets = []
        participants = []
        segments = []

        for source, target, participant, segment in self._data_iterator():

            # Store data
            sources.append(source)
            targets.append(int(target))
            participants.append(participant)
            segments.append(get_segment_name(segment))

        # Create DataFrame for all data
        self.df = pd.DataFrame(data={'sources': sources,
                                     'targets': targets,
                                     'participants': participants,
                                     'segments': segments})
        self.participant_set = set(participants)
        self.segment_set = set(segments)
        self.class_set = sorted(list(set(targets)))     # Ensure ordering is always the same

    def get_samples(self, batch_size, participants=None, segments=None):
        if participants is None:
            participants = self.participant_set
        if segments is None:
            segments = self.segment_set

        # Filter
        df = self.df[self.df.participants.isin(participants) &
                     self.df.segments.isin(segments)].copy()

        # Targets to onehot
        df.targets = targets_to_onehot(df.targets, self.class_set)

        # Create clip generator
        clip_generator = self.sampler.generate_samples(df)

        # Compute epoch steps
        clip_count = self.sampler.get_epoch_samples(df.sources)

        return pd.DataFrame([next(clip_generator) for _ in range(clip_count)])

    def get_generator(self, batch_size, participants=None, segments=None):
        if participants is None:
            participants = self.participant_set
        if segments is None:
            segments = self.segment_set

        # Filter
        df = self.df[self.df.participants.isin(participants) &
                     self.df.segments.isin(segments)].copy()

        # Targets to onehot
        df.targets = targets_to_onehot(df.targets, self.class_set)

        # Create clip generator
        clip_generator = self.sampler.generate_samples(df)

        # Compute epoch steps
        clip_count = self.sampler.get_epoch_samples(df.sources)

        steps = get_steps(clip_count, batch_size)

        return self.generate_batches(clip_generator, batch_size), steps

    def generate_batches(self, sources, batch_size):

        if isinstance(sources, pd.DataFrame):
            next_item = lambda x: next(x)[1]
            sources = sources.iterrows()
        else:
            next_item = next.__call__

        while True:
            source_batch = []
            target_batch = []

            while len(source_batch) < batch_size:
                item = next_item(sources)
                try:
                    source = self._preprocess(item.frames)
                    source_batch.append(source)
                    target_batch.append(item.targets)
                except Exception as e:
                    logger.warning('An error occured during preprocessing: {}'.format(str(e)))

            yield (np.array(source_batch), np.array(target_batch))

    def size(self):
        return self.df.sources.apply(lambda x: len(x)).sum()

    def get_input_shape(self):
        sample_size = self.sampler.sample_size()

        input_shape = (sample_size, *self.input_shape) if sample_size > 1 else self.input_shape

        return input_shape


    def balanced_class_weight(self, participants):
        targets = []
        participant_df = self.df[self.df.participants.isin(participants)]
        for i, row in participant_df.iterrows():
            targets.extend([row.targets] * len(row.sources))

        cls_weights = class_weight.compute_class_weight('balanced',
                                                        [0, 1],
                                                        targets)
        return cls_weights

    def leave_one_out_participant(self, participant):
        train = self.df[self.df.participants != participant].index.tolist()
        test = self.df[self.df.participants == participant].index.tolist()
        return train, test

    def get_classes(self, onehot_results):
        # Convert Onehot results to classes
        return onehot_to_targets(onehot_results, self.class_set)

    @staticmethod
    def create_chckpnt_filepath(model_name):
        """
        Checkpoints are overwritten when further trained.
        Warrning: model_name should contain version.
        """
        filepath = join(MODEL_CHECKPOINT_FOLDER, model_name)
        return filepath

    @staticmethod
    def create_tensorboard_dirpath(model_name):
        """
        Warrning: model_name should contain version.
        """
        dirpath = join(TENSORBOARD_FOLDER, model_name)
        return dirpath

    @staticmethod
    def create_log_filepath(model_name):
        """
        Warrning: model_name should contain version.
        """
        timestamp = datetime.now().isoformat(timespec='minutes')
        timestamp = timestamp.replace(':', '-')
        filepath = join(MODEL_LOG_FOLDER, '{}_{}'.format(model_name, timestamp))
        return filepath

    @staticmethod
    def create_predictions_filepath(model_name):
        """
        Warrning: model_name should contain version.
        """
        filepath = join(MODEL_EVAL_FOLDER, '{}_raw_results.pickle'.format(model_name))
        return filepath


class GrayDataset(Dataset):

    fps = 30
    input_shape = (360, 640, 1)

    @staticmethod
    def _is_desired_file(filepath):
        return filepath.endswith('_gray.png')

    def _data_iterator(self):
        for target, target_dirpath in get_subdirs(self.root_dirpath):
            for participant, particpant_dirpath in get_subdirs(target_dirpath):
                for sequence, seq_dirpath in get_subdirs(particpant_dirpath):
                    # Get filepaths
                    imgs = get_filepaths(seq_dirpath)

                    # Filter gray only
                    imgs = list(filter(self._is_desired_file, imgs))

                    # Sort
                    imgs = sorted(imgs)

                    # Make segment
                    segment = sequence[:-4]  # Cut off '_000'

                    yield imgs, target, participant, segment

    def _preprocess_img(self, filepath):
        img = imread(filepath, as_grey=True)
        img = np.atleast_3d(img)
        img = normalize(img)
        return img

    def _preprocess(self, source):
        if self.sampler.sample_size() > 1:    # Clips
            return np.array(
                [self._preprocess_img(filepath)
                 for filepath in source])
        else:                               # Frames
            return self._preprocess_img(source)



class GrayFlowDataset(GrayDataset):
    input_shape = (360, 640, 3)

    @staticmethod
    def _is_desired_file(filepath):
        return filepath.endswith('_flow.png')

    def _preprocess_img(self, source):

        # Read last img (this first to store gray)
        img = imread(source[-1])

        flow = img[:, :, :2]

        for filepath in reversed(source[:-1]):
            img = imread(filepath)
            flow += img[:, :, :2]

        img[:, :, :2] = flow

        return img


class GazeDataset(Dataset):

    fps = 60
    input_shape = (19,)

    def __init__(self, sampler, max_missing=0.):
        """
        Wrapper class for gaze dataset.
        :param sampler: DataSampler object which handles creating clips.
        :param max_missing : Maximum fraction of clip gaze data that can be
                             missing.
        """
        self.max_missing = max_missing
        super().__init__(sampler)

    @staticmethod
    def _get_missing_percent(source):
        # Get indices of Validity fields
        val_left_idx = 0
        val_right_idx = 0
        for idx, field in enumerate(LabData.GAZE_FIELDS):
            if field == 'ValidityLeft':
                val_left_idx = idx
            elif field == 'ValidityRight':
                val_right_idx = idx

        # Compute percentage
        missing_count = 0

        for record in source:
            if (record[val_left_idx] > 0) or (record[val_right_idx] > 0):
                missing_count += 1

        return missing_count / len(source)

    def _data_iterator(self):
        sources, targets, participants, segments = LabData().get_gaze()

        if self.max_missing > 0:
            indices = [i for i in range(len(sources))
                       if self._get_missing_percent(sources[i]) <= self.max_missing]
        else:
            indices = range(len(sources))

        for i in indices:
            yield sources[i], \
                  targets[i], \
                  participants[i], \
                  segments[i]

    def _preprocess(self, source):
        return source
