from time import time
from data.dataset import Dataset, GrayDataset, GrayFlowDataset
from data.data_sampling import UniformClipSampler, UniformFlowClipSampler, \
    RandomClipSampler


class ImgNotLoadedError(Exception):
    def __init__(self, filepath, img):
        self.filepath = filepath
        self.img = img
        super().__init__(
            'Failed to load file \'{}\'! Got \'{}\''.format(filepath, img))


def test_performance_clips():
    clip_count = 40

    sampler = UniformClipSampler(300, 1, 0.25)
    dataset = GrayDataset(sampler)
    generator, steps = dataset.get_generator(25)

    start = time()

    for _ in range(clip_count):
        next(generator)

    end = time() - start

    print('[Perf/{clip_c}clips] {clip_c} clips loaded in '
          '{time}s'.format(clip_c=clip_count, time=end))


def debug_gray_dataset():
    sampler = RandomClipSampler(10, 2, 0.25)
    data = GrayDataset(sampler)
    gen, steps = data.get_generator(25, ['P01'])
    for i in range(20*steps):
        next(gen)
    print('OK')


if __name__ == '__main__':
    debug_gray_dataset()
