import logging
import pickle
import re
from builtins import Exception
from os.path import dirname, join, isfile, isdir, basename

import numpy as np
import pandas as pd

logger = logging.getLogger(__name__)


# PATHS

DATA_FOLDER = join(dirname(dirname(dirname(__file__))), 'data', 'lab')
DATASET_FOLDER = join(DATA_FOLDER, 'dataset')
CACHE_FOLDER = join(dirname(DATA_FOLDER), 'cache')
TEST_DATA_FOLDER = join(dirname(DATA_FOLDER), 'test')


# Note: In thesis these test are named as follow:
#    Test 1 = Test 1
#    Test 3 = Test 2
#    Test 4 = Test 3
TEST_NAMES = ['Test 1', 'Test 3', 'Test 4']

CLASSES = ['disengagement', 'engagement']  # -1 = Not used
SEGMENTS = ['Neutral Face',  'Disengagement', 'Disengagement Mobile',
            'Disengagement Room', 'Mind Wandering', 'Interactive Site',
            'Mind Game', 'Video', 'RockNPoll - High Eng', 'RockNPoll - Low Eng',
            'Text - Diseng', 'Video - Diseng', 'Text - Mind Wandering',
            'Video - Mind Wandering', 'Text - Eng', 'Video - Eng']

SEGMENT_LABEL = {
    'Neutral Face': -1,
    'Text - Diseng': 0,
    'Video - Diseng': 0,
    'Disengagement': 0,
    'Disengagement Mobile': 0,
    'Disengagement Room': 0,
    'Text - Mind Wandering': 0,
    'Video - Mind Wandering': 0,
    'Mind Wandering': 0,
    'MWSelfReports': 0,
    'Text - Eng': 1,
    'Video - Eng': 1,
    'Interactive Site': 1,
    'Mind Game': 1,
    'Video': 1,
    'RockNPoll - High Eng': 1,
    'RockNPoll - Low Eng': -1,
    'Polygons - Low Eng': -1,
}


class SegmentHasNoLabelException(Exception):
    def __init__(self, segment_name):
        super().__init__('Segment \'%s\' has no associated label!' % segment_name)
        self.segment_name = segment_name


class InvalidSegmentName(Exception):
    def __init__(self, segment_name):
        super().__init__('Invalid segment name \'%s\'!' % segment_name)
        self.segment_name = segment_name


class InvalidClipFilename(Exception):
    def __init__(self, clip_filename):
        super().__init__('Invalid clip filename format \'%s\'!' % clip_filename)
        self.clip_filename = clip_filename


def _get_segment_label(segment):
    label = SEGMENT_LABEL.get(segment)

    if label is None:
        raise SegmentHasNoLabelException(segment)

    return label


def get_segment_name(segment):
    match = re.match(r'([^0-9 ]+(:? [^0-9 ]+)*)(?: [0-9]+)?$', segment)
    if match is None:
        raise InvalidSegmentName(segment)
    return match.group(1)


def get_label(filename = None, segment = None):
    assert (filename is not None) or (segment is not None)

    if segment is not None:
        segment = get_segment_name(segment)
        return _get_segment_label(segment)

    else:
        if not isfile(filename):
            raise FileNotFoundError(filename)
        filename = basename(filename)
        match = re.match(r'P[0-9]{2,3}_([^0-9 ]+(:? [^0-9 ]+)*)(?: [0-9]+)?\.mp4$', filename)
        if match is None:
            raise InvalidClipFilename(filename)
        segment = match.group(1)
        return _get_segment_label(segment)


class DirTreeNotInitialized(Exception):
    def __init__(self):
        super().__init__("Directory tree is not initialized!\n"
                         "Run \'setup_dirname\' function in order to resolve "
                         "this problem!")


def cache_output(filepath):
    """
    Decorator which caches func output if not already into pickle file.
    :param filepath: Path to output file.
    :return: func's output
    """
    def decorator(func):
        def func_wrapper(*args, **kwargs):
            if isfile(filepath):
                try:
                    logger.debug('Loading cached data from \'%s\'' % filepath)
                    with open(filepath, 'rb') as f:
                        return pickle.load(f)
                except FileNotFoundError as e:
                    raise DirTreeNotInitialized
            else:
                if isdir(dirname(filepath)):
                    ret = func(*args, **kwargs)
                    with open(filepath, 'wb') as f:
                        pickle.dump(ret, f)
                    logger.debug('Saved cached data to \'%s\'' % filepath)
                    return ret
                else:
                    raise DirTreeNotInitialized

        return func_wrapper
    return decorator


class LabData(object):
    RECORDS_FPS = 30
    # Display
    MEDIA_WIDTH = 1152
    MEDIA_HEIGHT = 720
    MEDIA_NORM_PARAM = 1 / MEDIA_WIDTH if MEDIA_WIDTH > MEDIA_HEIGHT else 1 / MEDIA_HEIGHT

    GAZE_FIELDS = ['ValidityLeft', 'ValidityRight',
                   'DistanceLeft', 'DistanceRight',
                   'PupilLeft', 'PupilRight',
                   # Left Eye pos (x,y,z)
                   'EyePosLeftX (ADCSmm)', 'EyePosLeftY (ADCSmm)',
                   'EyePosLeftZ (ADCSmm)',
                   # Right Eye pos (x,y,z)
                   'EyePosRightX (ADCSmm)', 'EyePosRightY (ADCSmm)',
                   'EyePosRightZ (ADCSmm)',
                   'GazePointIndex',
                   # Left Eye Gaze point (x,y)
                   'GazePointLeftX (ADCSpx)', 'GazePointLeftY (ADCSpx)',
                   # Right Eye Gaze point (x,y)
                   'GazePointRightX (ADCSpx)', 'GazePointRightY (ADCSpx)',
                   # Avg Gaze Point (x,y)
                   'GazePointX (ADCSpx)',
                   'GazePointY (ADCSpx)',
                   ]

    GAZE_POINT_FIELDS = ['GazePointLeftX (ADCSpx)',
                         'GazePointRightX (ADCSpx)',
                         'GazePointX (ADCSpx)',
                         'GazePointLeftY (ADCSpx)',
                         'GazePointRightY (ADCSpx)',
                         'GazePointY (ADCSpx)']
    EYE_POS_FIELDS = ['EyePosLeftX (ADCSmm)', 'EyePosLeftY (ADCSmm)',
                      'EyePosLeftZ (ADCSmm)',
                      'EyePosRightX (ADCSmm)', 'EyePosRightY (ADCSmm)',
                      'EyePosRightZ (ADCSmm)',
                      'DistanceLeft', 'DistanceRight']
    VALIDITY_FIELDS = ['ValidityLeft', 'ValidityRight']

    DICT_FIELDS_QUESTION = {
        'New Test': [],
        'Test 3': ['[Q rocknpoll-anglictina]Value', '[Q rocknpoll-videne]Value',
                   '[Q memgame-vyrusenie]Value', '[Q video-vyrusenie]Value',
                   '[Q video-videne]Value', '[Q mind_w-zahlbenie]Value',
                   '[Q - mind_w-prebratie]Value',
                   '[Q diseng_browsing-zacitanie]Value',
                   '[Q rocknpoll-vyrusenie]Value'],
        'Test 4': ['[Q01 anglictina]Value', '[Q02 nesignalizuje]Value',
                   '[Q03 vyrusenie]Value', '[Q04 pozna]Value'],
    }

    FIELD_DTYPE = {
        'StudioTestName': np.unicode_,
        'ParticipantName': np.unicode_,
        'RecordingName': np.unicode_,
        'SegmentName': np.unicode_,
        'SegmentStart': np.float32,
        'SegmentEnd': np.float32,
        'SegmentDuration': np.float32,
        'RecordingTimestamp': np.float32,
        'KeyPressEvent': np.unicode_,
        # Gaze
        'ValidityLeft': np.float32,
        'ValidityRight': np.float32,
        'DistanceLeft': np.float32,
        'DistanceRight': np.float32,
        'PupilLeft': np.float32,
        'PupilRight': np.float32,
        'EyePosLeftX (ADCSmm)': np.float32,
        'EyePosLeftY (ADCSmm)': np.float32,
        'EyePosLeftZ (ADCSmm)': np.float32,
        'EyePosRightX (ADCSmm)': np.float32,
        'EyePosRightY (ADCSmm)': np.float32,
        'EyePosRightZ (ADCSmm)': np.float32,
        'GazePointIndex': np.float32,
        'GazePointLeftX (ADCSpx)': np.float32,
        'GazePointLeftY (ADCSpx)': np.float32,
        'GazePointRightX (ADCSpx)': np.float32,
        'GazePointRightY (ADCSpx)': np.float32,
        'GazePointX (ADCSpx)': np.float32,
        'GazePointY (ADCSpx)': np.float32,
        'FixationIndex': np.float32,
        'SaccadeIndex': np.float32,
        'GazeEventType': np.unicode_,
        'GazeEventDuration': np.float32
    }

    FIELD_DEFAULT = {
        'StudioTestName': 'Unknown',
        'ParticipantName': 'Unknown',
        'RecordingName': 'Unknown',
        'SegmentName': 'Unknown',
        'SegmentStart': 0.,
        'SegmentEnd': 0.,
        'SegmentDuration': 0.,
        'RecordingTimestamp': 0.,
        'KeyPressEvent': 'Unknown',
        # Gaze
        'ValidityLeft': 1,
        'ValidityRight': 1,
        'DistanceLeft': -1000,
        'DistanceRight': -1000,
        'PupilLeft': -10,
        'PupilRight': -10,
        'EyePosLeftX (ADCSmm)': 0,
        'EyePosLeftY (ADCSmm)': 0,
        'EyePosLeftZ (ADCSmm)': -1000,
        'EyePosRightX (ADCSmm)': 0,
        'EyePosRightY (ADCSmm)': 0,
        'EyePosRightZ (ADCSmm)': -1000,
        'GazePointIndex': -1,
        'GazePointLeftX (ADCSpx)': -1000,
        'GazePointLeftY (ADCSpx)': -1000,
        'GazePointRightX (ADCSpx)': -1000,
        'GazePointRightY (ADCSpx)': -1000,
        'GazePointX (ADCSpx)': -1000,
        'GazePointY (ADCSpx)': -1000,
        'FixationIndex': -1,
        'SaccadeIndex': -1,
        'GazeEventType': 'Unknown',
        'GazeEventDuration': 0
    }

    def __init__(self):
        data_files = {}
        records_dirs = {}

        # Get paths to data.tsv files and record directories
        for test in TEST_NAMES:

            data_file = join(DATA_FOLDER, test, 'data.tsv')
            if not isfile(data_file):
                raise FileNotFoundError('File \'%s\' does not exist!\n'
                                        '%s incomplete!' % (data_file, test))
            data_files[test] = data_file

            records_dir = join(DATA_FOLDER, test, 'usercams')
            if not isdir(records_dir):
                raise NotADirectoryError('Dir \'%s\' does not exist!\n'
                                         '%s incomplete!' % (records_dir, test))
            records_dirs[test] = records_dir

        # Init parameters
        self.test_names = TEST_NAMES
        self.data_files = data_files
        self.records_dirs = records_dirs

    def _get_data_reader(self, test, cols):
        dtype = {field: self.FIELD_DTYPE[field] for field in cols}
        default = {field: self.FIELD_DEFAULT[field] for field in cols}

        reader = pd.read_csv(self.data_files[test], sep='\t', usecols=cols,
                             dtype=dtype, iterator=True, na_values=default,
                             decimal=',', chunksize=5000)
        return reader

    def _record_segment_data(self, cols):
        """
        Search every (record, segment) combination in every Test's data.tsv
        file and apply callback function.
        :param cols: Cols which should df contain (except record and segment).
        :return generator that generates (test, record, record_filepath,
                                          segment, df)
        """

        # Search data.tsv file for each test
        for test in self.test_names:
            reader = self._get_data_reader(test, cols)

            # Sequentially process chunks
            for chunk in reader:

                # Group by Record to distinguish between Segments
                for record, record_group in chunk.groupby(['RecordingName']):
                    record_filepath = join(self.records_dirs[test],
                                           '{}.mp4'.format(record))

                    if not isfile(record_filepath):
                        logger.warning('\'{}\' not found!\n'
                                       'data.tsv and records in '
                                       'usercams are inconsistent!'
                                       .format(record_filepath))

                    # Search (Record, Segment) combinations
                    for segment, segment_group in record_group.groupby(
                            ['SegmentName']):
                        yield (test, record, record_filepath, segment,
                               segment_group)

    @cache_output(join(CACHE_FOLDER, 'segment_tws.pickle'))
    def get_segment_tws(self):
        """
        Get time windows for each segment in each record.
        :return: [ (record_filepath, participant, segment_name, start_frame,
                    end_frame), ... ]
        """

        cols = ['StudioTestName', 'RecordingName', 'SegmentName',
                'ParticipantName', 'SegmentStart', 'SegmentEnd']
        tws = []
        visited = set()
        fpms = self.RECORDS_FPS * 0.001  # Convert to frames per millisecond

        for test, record, record_filepath, segment, df in \
                self._record_segment_data(cols):

            search_combination = (test, record, segment)

            # If unique comb.
            if search_combination not in visited:
                # Convert ms to frame index
                start = int(df['SegmentStart'].iloc[0] * fpms)
                end = int(df['SegmentEnd'].iloc[0] * fpms)
                participant = df['ParticipantName'].iloc[0]

                tws.append((record_filepath, participant, segment,
                            start, end))
                visited.add(search_combination)

        return tws

    @cache_output(join(CACHE_FOLDER, 'self_report_tws.pickle'))
    def get_self_report_tws(self, tw_size_in_ms):
        """
        Get time windows for each self report occurrence in each record.
        :return: [ (record_filepath, participant, segment_name, start_frame,
                    end_frame), ... ]
        """
        cols = ['StudioTestName', 'RecordingName', 'SegmentName',
                'ParticipantName', 'KeyPressEvent', 'RecordingTimestamp']
        tws = []
        fpms = self.RECORDS_FPS * 0.001  # Convert to frames per millisecond

        for test, record, record_filepath, segment, df in self._record_segment_data(cols):
            participant = df['ParticipantName'].iloc[0]

            # Filter self reports
            df = df[df['KeyPressEvent'] == 'Q']

            for press_time in df['RecordingTimestamp']:
                # Convert ms to frame index
                start = int((press_time - tw_size_in_ms) * fpms)
                end = int(press_time * fpms)

                tws.append((record_filepath, participant, segment,
                            start, end))

        return tws

    def annotate_tws(self, seg_tws, sr_tws):
        """
        Annotate segment and self_report time windows.
        Time windows format: [ (record_filepath, participant, segment_name,
                                start_frame, end_frame), ..]
        :param seg_tws: Segment time windows
        :param sr_tws: Self report time windows
        :return: Annotated time windows (contains extra 'label' value).
        """
        seg_tws = [(file, participant, seg, start, end, get_label(segment=seg))
                   for file, participant, seg, start, end in seg_tws
                   if get_label(segment=seg) != -1]

        sr_label = get_label(segment='MWSelfReports')
        if sr_label >= 0:
            sr_tws = [(file, participant, seg, start, end, sr_label)
                      for file, participant, seg, start, end in sr_tws]
        else:
            sr_tws = None

        return seg_tws, sr_tws

    def _normalize_gaze(self, df):
        # Normalize Gaze Point
        df[self.GAZE_POINT_FIELDS] = df[self.GAZE_POINT_FIELDS] * self.MEDIA_NORM_PARAM

        # Normalize Eye Points
        df[self.EYE_POS_FIELDS] = df[self.EYE_POS_FIELDS] * 0.001  # Convert to meters

        # Validity to <0-val, 1-inval>
        df[self.VALIDITY_FIELDS] = df[self.VALIDITY_FIELDS]\
            .applymap(lambda x: 1 if x > 0 else 0)

        return df

    @cache_output(join(CACHE_FOLDER, 'gaze.pickle'))
    def get_gaze(self):
        """
        Get all preprocessed gaze data.
        :return: sources, targets, parcipant_groups
            Note: Values in sources are ordered as columns in
                  LabData.GAZE_FIELDS.
        """
        cols = self.GAZE_FIELDS + ['RecordingName', 'SegmentName',
                                   'ParticipantName', 'KeyPressEvent']
        label_sr = get_label(segment='MWSelfReports')

        # Results
        sources = []
        targets = []
        participants = []
        segments = []

        # Search data.tsv file for each test
        for test in self.test_names:
            reader = self._get_data_reader(test, cols)

            # Sequentially process chunks
            for chunk in reader:
                # Get targets
                chunk['label'] = chunk['SegmentName'].apply(lambda x: get_label(segment=x))
                chunk = chunk[chunk['label'] >= 0]

                # Transformations
                chunk = chunk.fillna(self.FIELD_DEFAULT)
                chunk = self._normalize_gaze(chunk)

                # Mark self reports
                if label_sr >= 0:
                    mask = chunk['KeyPressEvent'] == 'Q'
                    chunk.loc[mask, 'SegmentName'] = 'MWSelfReports_' + chunk.loc[mask, 'SegmentName']

                # Store sequences (groupbying as is)
                for record, record_df in chunk.groupby('RecordingName', sort=False):
                    participant = record_df['ParticipantName'].iloc[0]

                    for segment, segment_df in record_df.groupby('SegmentName', sort=False):
                        target = segment_df['label'].iloc[0]

                        sources.append(segment_df[self.GAZE_FIELDS].values)
                        targets.append(target)
                        participants.append(participant)
                        segments.append(segment)

        return sources, targets, participants, segments
