from os import listdir
from os.path import join, isdir, isfile


def get_subdirs(dirpath):
        return [(filename, join(dirpath, filename))
                for filename in listdir(dirpath)
                if isdir(join(dirpath, filename))]


def get_filepaths(dirpath):
    return [join(dirpath, filename)
            for filename in listdir(dirpath)
            if isfile(join(dirpath, filename))]
