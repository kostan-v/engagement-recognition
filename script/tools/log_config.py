import datetime
from os.path import join, dirname

LOG_FOLDER = join(dirname(dirname(dirname(__file__))), 'logs')
LOG_FILE_FORMAT = LOG_FOLDER + '/{}-{}.log'


def get_log_filename(script_name):
    return LOG_FILE_FORMAT.format(datetime.datetime.now().isoformat(timespec='minutes'), script_name)


def setup_logging(logging, log_name):
    fileHandler = logging.FileHandler(get_log_filename(log_name))
    consoleHandler = logging.StreamHandler()
    logging.basicConfig(level=logging.DEBUG,
                        format="%(asctime)s [%(name)-16.16s] [%(levelname)-5.5s]  %(message)s",
                        handlers=[fileHandler, consoleHandler])
    return logging.getLogger(log_name)
