import cv2
import logging
from skvideo.io import FFmpegReader, FFmpegWriter, ffprobe


logger = logging.getLogger(__name__)


IO_HANDLER = 'cv'


def get_supported_io():
    global IO_HANDLER
    return IO_HANDLER


class VideoInputSKVIDEO:
    """Input handler wrapper for scikit-video's FFmpegReader."""

    fps = 62.5

    def __init__(self, filename=None):
        if filename is None:
            self.reader = None
            self.generator = None
            self.meta = None
            self.nframes = 0
            self.fps = 0.
            self._is_opened = False
        else:
            self.open(filename)

    def open(self, filename):
        self.reader = FFmpegReader(filename)

        # Check if opened
        next(self.reader.nextFrame())

        self.generator = self.reader.nextFrame()
        self.meta = ffprobe(filename)
        self.nframes, _, __, ___ = self.reader.getShape()

        if 'video' in self.meta.keys():
            self.meta = self.meta['video']
            fps_str = self.meta['@avg_frame_rate'].split(sep='/')
            self.fps = float(fps_str[0]) / float(fps_str[1])
        else:
            logger.error("Failed to load or non exist video meta information \'%s\'!" % filename)
            logger.warning("Using default %.2f fps." % VideoInputSKVIDEO.fps)
            self.fps = VideoInputSKVIDEO.fps

        self._is_opened = True

    def is_opened(self):
        return self._is_opened
        
    def read(self, frame=None):
        """frame does nothing - compatibility reasons (VideoInputCV2)"""
        assert self._is_opened
        frame = next(self.generator)
        self._is_opened = frame is not None

        return self._is_opened, frame

    def release(self):
        if self.reader is not None:
            self.reader.close()
        self.reader = None
        self.nframes = 0
        self.fps = 0.
        self._is_opened = False


class VideoInputCV2:
    """Input handler wrapper for opencv's VideoCapture."""

    def __init__(self, filename=None):
        if filename is None:
            self.reader = None
            self.nframes = 0
            self.fps = 0.
            self.frame_size = (0, 0)
        else:
            self.open(filename)

    def open(self, filename):
        self.reader = cv2.VideoCapture(filename)
        if not self.reader.isOpened():
            raise Exception('Failed to open video \'%s\'!' % filename)
        self.nframes = int(self.reader.get(cv2.CAP_PROP_FRAME_COUNT))
        self.fps = self.reader.get(cv2.CAP_PROP_FPS)
        self.frame_size = (int(self.reader.get(cv2.CAP_PROP_FRAME_WIDTH)),
                           int(self.reader.get(cv2.CAP_PROP_FRAME_HEIGHT)))

    def is_opened(self):
        if self.reader is not None:
            return self.reader.isOpened()
        else:
            return False

    def read(self, frame=None):
        return self.reader.read(frame)

    def go_to_frame(self, i):
        self.reader.set(cv2.CAP_PROP_POS_FRAMES, i)

    def release(self):
        if self.is_opened():
            self.reader.release()
        self.reader = None
        self.nframes = 0
        self.fps = 0.
        self.frame_size = (0, 0)


def get_input_stream(filename=None, io=None):
    """
    Factory function for input handlers.
    If OpenCv input is supported (--cvinput argument)
    returns wrapper of cv2.VideoCapture, otherwise
    returns wrapper of skvideo.io.FFmpegReader.
    """
    if io is None:
        io = get_supported_io()
    if io == 'cv':
        return VideoInputCV2(filename)
    else:
        return VideoInputSKVIDEO(filename)


class VideoOutputSKVIDEO:
    """Output handler wrapper for scikit-video's FFmpegWriter. """

    def __init__(self, filename=None, frame_size=(0,0), fps=0., pix_fmt=''):
        if filename is None:
            self.writer = None
            self.filename = ''
            self.frame_size = frame_size
            self.fps = fps
            self.pix_fmt = pix_fmt
            self._is_opened = False
        else:
            self.open(filename, frame_size, fps, pix_fmt)

    def open(self, filename, frame_size=None, fps=None, pix_fmt=None):
        if frame_size is not None:
            self.frame_size = frame_size
        if fps is not None:
            self.fps = fps
        if pix_fmt is not None:
            self.pix_fmt = pix_fmt

        outputdict = {
            '-s': "{w:}x{h:}".format(w=self.frame_size[0],
                                     h=self.frame_size[1]),
            '-r': str(self.fps),
        }
        inputdict = {
            '-pix_fmt': self.pix_fmt,
        }

        self.writer = FFmpegWriter(filename, inputdict=inputdict,
                                   outputdict=outputdict)
        self.filename = filename
        self._is_opened = True

    def is_opened(self):
        return self._is_opened

    def write(self, frame):
        assert self._is_opened
        self.writer.writeFrame(frame)

    def release(self):
        if self.writer is None:
            self.writer.close()
        self.writer = None
        self._is_opened = False


class VideoOutputCV2:
    """Output handler wrapper for OpenCV's VideoWriter."""

    def __init__(self, filename=None, frame_size=None, fps=None, pix_fmt=None):
        if filename is None:
            self.writer = None
            self.filename = None
            self.frame_size = frame_size
            self.fps = fps
            self.pix_fmt = pix_fmt
            self._is_opened = False
        else:
            self.open(filename, frame_size, fps, pix_fmt)

    def open(self, filename, frame_size=None, fps=None, pix_fmt=None):
        if frame_size is not None:
            self.frame_size = frame_size
        if fps is not None:
            self.fps = fps
        if pix_fmt is not None:
            self.pix_fmt = pix_fmt

        self.writer = cv2.VideoWriter(filename=filename,
                                      fourcc=cv2.VideoWriter_fourcc(*'XVID'),
                                      fps=self.fps,
                                      frameSize=self.frame_size,
                                      isColor=self.pix_fmt != 'gray' if self.pix_fmt is not None else True)

        if not self.writer.isOpened():
            raise Exception('Failed to open writer!')

        self.filename = filename
        self._is_opened = True

    def is_opened(self):
        if self.writer is not None:
            return self.writer.isOpened()
        else:
            return False

    def write(self, frame):
        assert self._is_opened
        self.writer.write(frame)

    def release(self):
        if self.writer is None:
            self.writer.release()
        self.writer = None
        self._is_opened = False


def get_output_stream(filename=None, frame_size=None, fps=None, pix_fmt=None, io=None):
    """
    Factory function for output handlers.
    If OpenCv input is supported
    returns wrapper of cv2.VideoWriter, otherwise
    returns wrapper of skvideo.io.FFmpegWriter.
    """
    if io is None:
        io = get_supported_io()
    if io == 'cv':
        return VideoOutputCV2(filename, frame_size, fps, pix_fmt)
    else:
        return VideoOutputSKVIDEO(filename, frame_size, fps, pix_fmt)
