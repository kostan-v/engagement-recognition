from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout, LSTM, BatchNormalization

from keras.layers.wrappers import TimeDistributed
from keras.models import Sequential
from keras.optimizers import Adam
from keras.regularizers import l1, l2

import logging


logger = logging.getLogger(__name__)


class ModelCatalog:

    @staticmethod
    def cnn(input_shape, nb_classes):
        l2_l = 1e-7

        model = Sequential()

        model.add(Conv2D(32, (7, 7), strides=(2, 2), activation='relu', padding='same',
                         kernel_regularizer=l2(l2_l), input_shape=input_shape))
        model.add(BatchNormalization())
        model.add(Conv2D(32, (3, 3), activation='relu', padding='same', kernel_regularizer=l2(l2_l)))
        model.add(BatchNormalization())
        model.add(MaxPooling2D((2, 2), strides=(2, 2)))

        model.add(Conv2D(64, (3, 3), padding='same', activation='relu', kernel_regularizer=l2(l2_l)))
        model.add(BatchNormalization())
        model.add(Conv2D(64, (3, 3), padding='same', activation='relu', kernel_regularizer=l2(l2_l)))
        model.add(BatchNormalization())
        model.add(MaxPooling2D((2, 2), strides=(2, 2)))

        # model.add(Conv2D(128, (3, 3), padding='same', activation='relu',
        #                  kernel_regularizer=l2(l2_l)))
        # model.add(Conv2D(128, (3, 3), padding='same', activation='relu',
        #                  kernel_regularizer=l2(l2_l)))
        # model.add(MaxPooling2D((2, 2), strides=(2, 2)))

        model.add(Flatten())

        model.add(Dropout(0.5))

        model.add(Dense(512, activation='relu', kernel_regularizer=l2(l2_l)))

        model.add(Dense(nb_classes, activation='softmax'))

        return model

    @staticmethod
    def cnn_lstm_small(input_shape, nb_classes):
        """
        l2 - in "https://arxiv.org/pdf/1704.06756.pdf" the best was 1e-7
        """
        cnn_regul = l2(1e-7)
        lstm_regul = l2(1e-7)

        model = Sequential()
        model.add(TimeDistributed(Conv2D(32, (7, 7), strides=(2, 2),
                                         activation='relu', padding='same',
                                         kernel_regularizer=cnn_regul
                                         ),
                                  input_shape=input_shape))
        model.add(TimeDistributed(BatchNormalization()))
        model.add(TimeDistributed(Conv2D(32, (3, 3),
                                         activation='relu', padding='same',
                                         kernel_regularizer=cnn_regul
                                         )))
        model.add(TimeDistributed(BatchNormalization()))
        model.add(TimeDistributed(MaxPooling2D((2, 2), strides=(2, 2))))

        model.add(TimeDistributed(Conv2D(64, (3, 3),
                                         padding='same', activation='relu',
                                         kernel_regularizer=cnn_regul
                                         )))
        model.add(TimeDistributed(BatchNormalization()))
        model.add(TimeDistributed(Conv2D(64, (3, 3),
                                         padding='same', activation='relu',
                                         kernel_regularizer=cnn_regul
                                         )))
        model.add(TimeDistributed(BatchNormalization()))
        model.add(TimeDistributed(MaxPooling2D((2, 2), strides=(2, 2))))

        model.add(TimeDistributed(Flatten()))

        model.add(Dropout(0.5))

        model.add(LSTM(128, return_sequences=False, dropout=0.5,
                       kernel_regularizer=lstm_regul))
        model.add(Dense(nb_classes, activation='softmax'))

        return model

    @staticmethod
    def lstm_gaze(input_shape, nb_classes):

        model = Sequential()

        model.add(LSTM(192, return_sequences=False, dropout=0.5,
                       activity_regularizer=l1(0.0001), input_shape=input_shape))

        model.add(Dense(nb_classes, activation='softmax'))

        return model


def get_model(name, input_shape, learning_rate):
    nb_classes = 2
    metrics = ['accuracy']

    if name == 'cnn-lstm-small':
        model = ModelCatalog.cnn_lstm_small(input_shape, nb_classes)
    elif name == 'cnn':
        model = ModelCatalog.cnn(input_shape, nb_classes)
    elif name == 'lstm-gaze':
        model = ModelCatalog.lstm_gaze(input_shape, nb_classes)
    else:
        raise NotImplementedError

    # Now compile the network.
    optimizer = Adam(lr=learning_rate)
    model.compile(loss='binary_crossentropy', optimizer=optimizer,
                  metrics=metrics)

    print(model.summary())
    return model
