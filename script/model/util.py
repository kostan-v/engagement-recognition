import logging

import pickle
from os.path import join, isfile
from keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping, \
    CSVLogger

from data.dataset import Dataset, MODEL_DATA_FOLDER

logger = logging.getLogger(__name__)

MODEL_PARAMETERS_FOLDER = join(MODEL_DATA_FOLDER, 'params')


def make_model_name(model_type, model_version):
    return '{}_v{:02d}'.format(model_type, model_version)


class ModelParameters:

    @staticmethod
    def get_params_path(model_name):
        return join(MODEL_PARAMETERS_FOLDER, model_name + '.pickle')

    @staticmethod
    def load(model_name):
        with open(ModelParameters.get_params_path(model_name), 'rb') as file:
            self_dict = pickle.load(file)
            return ModelParameters(**self_dict)

    def save(self):
        filepath = ModelParameters.get_params_path(self.model_name)
        if isfile(filepath):
            logger.warning('Overwriting \'{}\'!'.format(filepath))
        with open(filepath, 'wb') as file:
            pickle.dump(self.__dict__, file)
            logger.info('Params saved to \'{}\'.'.format(filepath))

    def __init__(self,
                 epochs,
                 model_type,
                 model_version,
                 model_name,
                 data_type,
                 overlap,
                 sample_every_i_frame,
                 clip_length,
                 batch_size,
                 learning_rate=None,
                 remove_missing=False,
                 max_missing=0.,
                 balanced=False,
                 val_participant=None
                 ):
        if max_missing > 0:
            remove_missing = True
        self.epochs = epochs
        self.model_type = model_type
        self.model_version = model_version
        self.model_name = model_name
        self.data_type = data_type
        self.overlap = overlap
        self.sample_every_i_frame = sample_every_i_frame
        self.clip_length = clip_length
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.remove_missing = remove_missing
        self.max_missing = max_missing
        self.balanced = balanced
        self.val_participant = val_participant

    def __str__(self):
        header = super().__str__()
        attribs = dict(
            epochs = self.epochs,
            model_type = self.model_type,
            model_version = self.model_version,
            model_name = self.model_name,
            data_type = self.data_type,
            overlap = self.overlap,
            sample_every_i_frame = self.sample_every_i_frame,
            clip_length = self.clip_length,
            batch_size = self.batch_size,
            learning_rate = self.learning_rate,
            max_missing = self.max_missing,
            balanced = self.balanced,
            val_participant = self.val_participant,
        )

        return '\n'.join([header, 'ModelParameters:'] +
                         ['\t{}={}'.format(key, val)
                          for key, val in attribs.items()])


def load_predictions_results(model_name):
    filename = Dataset.create_predictions_filepath(model_name)
    with open(filename, 'rb') as f:
        return pickle.load(f)


def train(dataset, train_participants, test_participants, batch_size, model, epochs,
          callbacks):
    train_generator, train_steps = dataset.get_generator(batch_size, participants=train_participants)
    val_generator, val_steps = dataset.get_generator(batch_size, participants=test_participants)

    cls_weights = dataset.balanced_class_weight(train_participants)

    model.fit_generator(
        train_generator,
        steps_per_epoch=train_steps,
        epochs=epochs,
        shuffle=True,
        verbose=1,
        validation_data=val_generator,
        validation_steps=val_steps,
        class_weight=cls_weights,
        callbacks=callbacks,
    )


def get_callbacks(model_name, batch_size):
    checkpoints = ModelCheckpoint(
        filepath=Dataset.create_chckpnt_filepath(model_name),
        verbose=1,
        save_best_only=True)

    tb = TensorBoard(
        log_dir=Dataset.create_tensorboard_dirpath(model_name),
        batch_size=batch_size)

    csv_log = CSVLogger(filename=Dataset.create_log_filepath(model_name))

    early_stopping = EarlyStopping(monitor='val_loss', patience=15)

    return [checkpoints, tb, early_stopping, csv_log]
