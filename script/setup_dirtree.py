import logging
from os import makedirs
from os.path import isdir

from data.lab_data import DATA_FOLDER, CACHE_FOLDER, TEST_DATA_FOLDER
from data.dataset import MODEL_CHECKPOINT_FOLDER, TENSORBOARD_FOLDER, \
    MODEL_LOG_FOLDER
from model.util import MODEL_PARAMETERS_FOLDER
from tools.log_config import LOG_FOLDER

logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s [%(name)-16.16s] [%(levelname)-5.5s]  "
                           "%(message)s",)
logger = logging.getLogger(__name__)

logger.info('Setting up directory tree...')
folders = [DATA_FOLDER, CACHE_FOLDER, TEST_DATA_FOLDER, LOG_FOLDER,
           MODEL_CHECKPOINT_FOLDER, TENSORBOARD_FOLDER, MODEL_LOG_FOLDER,
           MODEL_PARAMETERS_FOLDER]

for folder in folders:
    if not isdir(folder):
        makedirs(folder)
        logger.info('Created \'%s\'' % folder)
    else:
        logger.info('Skipped \'%s\'' % folder)

logger.info('Done.')
